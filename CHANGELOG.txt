//------------------------------------------------------------------------------
v1.1.0 - 30 Nov, 21
    - [NEW] Add the "--force" option so the tool creates the clone directory
            if doesn't exits.
    - [NEW] Add .ark_bump file.

//------------------------------------------------------------------------------
v1.0.0
    ...